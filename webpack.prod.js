const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'branding-asset-primitives.js',
    library: 'branding-asset-primitives',
    libraryTarget: 'umd',
    publicPath: '/',
  },
  externals: [
    '@material-ui/core',
    '@material-ui/icons',
    'classnames',
    'jwt-decode',
    'plotly.js',
    'prop-types',
    'react',
    'react-csv-reader',
    'react-dom',
    'react-hot-loader',
    'react-plotly.js',
    'react-router-dom',
    'react-select',
    'react-timestamp',
  ],
});
