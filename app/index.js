import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import Root from './Root';

// Generated using Material-UI Color Tool 'https://material.io/tools/color/'
const palette = {
  type: 'light',
  background: {
    default: 'rgb(240, 242, 245)',
  },
  secondary: {
    main: '#c96a7c',
    dark: '#953c50',
    light: '#fe9aab',
  },
  primary: {
    main: '#606191',
    dark: '#343763',
    light: '#8f8ec2',
  },
};

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
        <Component />
    </AppContainer>,
    document.getElementById('root'),
  );
};

render(Root);

if (module.hot) {
  module.hot.accept('./Root', () => {
    const newApp = require('./Root').default;
    render(newApp);
  });
}
