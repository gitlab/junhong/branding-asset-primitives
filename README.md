# Branding Asset Primitives #

[![pipeline status](https://apps.risksciences.ucla.edu/gitlab/girs/branding-asset-primitives/badges/develop/pipeline.svg)](https://apps.risksciences.ucla.edu/gitlab/girs/branding-asset-primitives/commits/develop)

Unified institute wide branding assets - source and exported files for digital and print use.

## Dependencies
Refer to `package.json`
